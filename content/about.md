+++
title = "About Us"
description = "An introduction to Zone 5"
date = "2023-02-16"
aliases = ["introduction"]
author = "J5 Media"
+++

## Introduction

This website is designed and maintained by Gotam Dahiya to store and showcase any notes and images recorded while playing DCS and Arma 3. 

Most notes will be about mission making, mods and so forth. Just stuff to remember. 

Images will be ones collected over play-time. No post-processing because I am lazy that way.