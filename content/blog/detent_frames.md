+++
author = "(SN)Orion | vCSG-3 | Hornet School"
title = "Detents with VKB Stecs"
image = "img/slide/1.png"
date = "2024-06-02T00:00:00"
description = "Which planes have which frames and how are the detents setup"
tags = [
    "scripting"
]
+++

## F/A-18C Hornet
1. Frame : Red
2. Idle buttons, Pulse : 
    1. Left Idle off : 89
    2. Right Idle Off : 90
    3. Left Idle On : 91
    4. Right Idle On : 92
3. Afterburner Finger Lift:
    1. Left Finger Lift : 93
    2. Right Finger Lift : 94

## Mosquito
1. Frame : Blue
2. Throttle Press handle:
    1. Left Handle : 97
    2. Right Handle : 98

## M2000C
1. Frame : White
2. No buttons but percentage
    1. Idle : 5%
    2. Afterburner : 85%

## Helicopters
1. Frame : Black

## AV-8B Harrier
1. Frame : White
2. Throttle Press handle:
    1. Cutoff : 5%
    2. Full throttle : 85%

3. Frame : Green
4. Throttle Press handle:
    1. Cutoff : 5%
    2. Full throttle cut : 90%