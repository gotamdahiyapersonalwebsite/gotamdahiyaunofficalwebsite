+++
author = "(SN)Orion | vCSG-3 | Hornet School"
title = "Map and Plane Distribution"
image = "img/slide/2.png"
date = "2024-04-19T00:00:00"
description = "Distribution of the maps and planes to be used on them."
tags = [
    "campaigns"
]
+++

| Map Name | Real World Location | Plane List | Remarks | 
| :------: | :-----------------: | :--------: | :-----: |
| Caucasus | Georgia, parts of Turkey, Armenia, and Crimea | None | Old map and kinda bored with it. |
| Falklands | Southern Argentina, Chile, and the Falklands Islands | AV8NA Harrier | Perfect fit for long distance flights and also a nice theater |
| Sinai | Southern Israel, North-Eastern Egypt, Western Jordan | M-2000C Mirage | Lots of runways to land on and create ranges to practice with |
| Marianas | Marianas Islands | Hornet | Lots and lots of water with islands making perfect targets |
| Nevada | Arizona, training area for the US Air Force | All helicopters | Mountains to hide behind and practice pop-up attacks, also flying through vegas |
| Syria | Western Syria, Cyprus, Southern coastline of Turkey, Northern Israel | AJS-37 Viggen | Low level flying is fun here and perfect for the Viggen |
| Persian Gulf | The Hormuz Strait | All mod planes, A4E, Hercules, Helicopters | Provides a large enough area to test and play around in.
| Afghanistan | The entire region of Afghanistan, with parts of the other stan countries | All helicopters | Really cool place to do some helicopter missions and harrier strikes, high average altitude |