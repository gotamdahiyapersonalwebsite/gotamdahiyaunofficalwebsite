+++
author = "(SN)Orion | vCSG-3 | Hornet School"
title = "General MOOSE Scripting"
image = "img/slide/1.png"
date = "2024-03-09T00:00:00"
description = "Basic description on using the MOOSE scripting environment"
tags = [
    "scripting"
]
weight = 1
+++

## Introduction

Mission Object Oriented Scripting Engine is a collection of functions which are basically a wrapper for the base functions present in DCS World. It follows OOP principles allowing for a better scripting environment for DCS World.

### Downloading and Loading MOOSE

MOOSE can be downloaded from [Flight-Control MOOSE_INCLUDE](https://github.com/FlightControl-Master/MOOSE_INCLUDE) in the `develop` branch.

The Moose_.lua script present in the static folder will have to be loaded into the trigger list in DCS. The following table shows the Trigger, Condition and Action.

| Trigger | Condition | Action |
| ------- | --------- | ------ |
| 1 ONCE (Load MOOSE, No Event) | TIME MORE (5) | DO SCRIPT FILE(`path_to_script_file`) |

### Main Use Cases

1. Using the `NAVYGROUP` to control naval groups containing carriers or Landing Helicopter Assault (LHAs) to turn into the wind for recovery operations, turn on and off the beacons associated with the ship, and also handle the lights for the carrier/LHA shifting between Auto, Navigation, Launch, Recovery and off.
2. Using the core menu to randomly spawn targets for practicing A2A or A2G.
3. Creating random air traffic for any mission.
4. Creating ranges for A2G and publishing results.

## Scripting

### Naval Groups

MOOSE Documentation : [Ops.NAVYGROUP](https://flightcontrol-master.github.io/MOOSE_DOCS_DEVELOP/Documentation/Ops.NavyGroup.html)

In a script it is easier to create a table/dictionary to contain all the necessary groups. This allows for common functions to accept any group depending upon the command menu option used.

**Using Airboss**

Navy groups allow for a better control of the carrier for ariboss operations, activating beacons and lights. The beacons and lights have to be set as triggered actions for the carrier group otherwise they cannot be used. The following table shows an example for deactivating and activating a beacon respectively.

| Trigger | Condition | Action |
| ------- | --------- | ------ |
| 3 SWITCHED CONDITION | FLAG IS TRUE (FLAG VALUE A) | FLAG OFF (FLAG VALUE B) & AI TASK PUSH (Triggered action)
| 3 SWITCHED CONDITON | FLAG IS TRUE (FLAG VALUE B) | FLAG OFF (FLAG VALUE A) & AI TASK PUSH (Triggered action)

The above table can also be used for other activations via the command menu. It does not only apply to the Navy Groups

To turn the carrier into the wind the following code block is required, 

```lua
function startRecovery(recovery_parameters)

   local recovery_time = recovery_parameters[1]
   local ship_name = recovery_parameters[2]

   local time_now = timer.getAbsTime()
   local time_end = time_now + recovery_time*60

   local steaming_into_wind = ship_dictionary[ship_name]["NavyGroup"]:IsSteamingIntoWind()

   if (steaming_into_wind) then
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." is already steaming into the wind till "..ship_dictionary[ship_name]["TimeRecoveryEnd"], 15, "INFO"):ToAll()
   else
      local timer_recovery_start = UTILS.SecondsToClock(time_now, false)
      timer_recovery_end = UTILS.SecondsToClock(time_end, false)
      ship_dictionary[ship_name]["TimeRecoveryEnd"] = timer_recovery_end

      ship_dictionary[ship_name]["NavyGroup"]:AddTurnIntoWind(timer_recovery_start, timer_recovery_end, 25, false, -9)
      MESSAGE:New("99 "..ship_dictionary[ship_name]["ShipCallsign"].." turning, at time "..timer_recovery_start.." until "..timer_recovery_end, 15, "INFO"):ToAll()
   end
end
```

The above code block uses a dictionary to store the Navy Groups along with their callsigns, and time till recovery end. `AddTurnIntoWind` turns the carrier group into the wind for a selected period of time which can be specefied in the command menu option. `IsSteamingIntoWind` checks if the carrier group is already steaming into the wind or not. For each Navy Groups the checks and time till recovery end can be different so that is why each Navy Group has it's own dictionary

### Menu Command Options

MOOSE Documentation : [Core.MENU](https://flightcontrol-master.github.io/MOOSE_DOCS/Documentation/Core.Menu.html)

MOOSE provides a better functionality to create menus in the default DCS menu than triggers in the mission editor. It allows for creating menus and their sub-menus faster and easier.

There are two functions, `MENU_COALITION` and `MENU_COALITION_COMMAND`. The first one only sets up the menu command and does not execute any function. The second one sets up the menu command and will execute a function.

Dictionaries have to be used to pass arguments back to the function in `MENU_COALITION_COMMAND`. MOOSE does not accept any other method

### Random Air Traffic

MOOSE Documentation : [Functional.RAT](https://flightcontrol-master.github.io/MOOSE_DOCS_DEVELOP/Documentation/Functional.RAT.html)

This is a very simple setup and uses late activated planes as templates. No matter which plane is set down, the coalition of the plane can be defined in the mission script itself. The primary commands are listed below,

1. `RAT:New(<template_group_name>)` : Group name of a late-activated plane/helicopter. The template can be placed anywhere on the map as long as it is late activated. Preferrably they should be placed near a corner so that they are not confused as other placebables.
2. `SetCoalitionAircraft` : Sets the coalition of the aircraft, either neutral, red or blue.
3. `SetCoalition` : Airplanes will only land/spawn at their coalition airports or neutral if allowed by this command. Same applies for coalition of aircraft and neutral airports, sameonly for only the coalition airports and neutral for only neutral airports. Defaults to same.
4. `ContinueJourney` : After the plane/helicopter lands, it will take off from the same FARP/Airbase and proceed to a random airbase satisfying coalition and destination parameters.
5. `SetMinDistance` : Minimum distance to be travelled between spawn and destination airport. This is in kilometers.
6. `Spawn` : Number of aircraft to be spawned in across the entire map. The value is maintained throughout the run of the mission.
7. `SetDeparture` : Set the spawn points for the aircraft, can also accept trigger zones. It takes a dictionary of values.
8. `SetDestination` : Set destination zones, can also set trigger zones. It takes a dictionary of values.
9. `SetTakeoff` : Determine whether engines should be hot or cold on spawn.
10. `SetEPLRS` : Turns on the datalink for the random air traffic plane.
11. `SetFLmin` : Sets the minimum flight level in 100s of feet.
12. `SetFLmax` : Sets the maximum flight level in 100s of feet.
13. `SetFlcruise` : Sets the cruising altitude in 100s of feet.
14. `RadioFrequency` : Sets the radio frequency of the spawned aircraft. Defauls to 251.000 Mhz or whatever was set in the mission editor for the template.
15. `RadioModulation` : Sets the modulation for the frequency, takes either FM or AM.
16. `RadioON` : Turns on the radio for transmission.
17. `ExcludedAirports` : Table of of airports to be excluded from the pattern.

### Range Control

MOOSE Documentation : [Functional.RANGE](https://flightcontrol-master.github.io/MOOSE_DOCS_DEVELOP/Documentation/Functional.Range.html)

This is designed for creating ranges for praciting air-to-ground munitions for any plane. It can also be used for helicopters as they are registered entering and leaving the ranges. The targets for any bombing/AGM are to be put in a table/dictionary.

1. `RANGE:New(<range name>)` : Initialises a new range with the specified range name.
2. `RANGE:AddBombingTargets(<targets_table>, <good_hit>)` : The targets for AGM/bombs is specified as a table/dictionary with the second argument being the radius below which a good/excellent hit is counted.
3. `RANGE:SetRangeRadius()` : Sets the radius in which results are recorded. It takes the first target in the target dictionary as the centre. So accordingly place a centre target or place the targets close to each other. If there is a massive range, suggest to split in into smaller ranges. This distance is in kilometers.
4. `RANGE:SetMaxStrafeAlt()` : Sets the maximum altitude for a strafing run. Does not apply to bombing ranges. This is specified in meters.
5. `RANGE:SetMessageTimeDuration()` : Sets the message duration in seconds. Some messages from RANGE by default stay on screen for 30 seconds. This can reduce/increase that time.
6. `RANGE:TrackRocketsOFF()` : Disables rocket tracking for a specific range.
7. `RANGE:AddStrafePit(strafe_pit_table, box_length, box_width, heading, inversepass, goodpass, foulline_distance)` : This add a strafing pit to the range.
   a. strafe_pit_table : Table/Dictionary containing the strafe target and foulline marker.
   b. box_length : Length of the approach box in meters
   c. box_width : Width of the approach box in meters
   d. heading : Heading on which to approach
   e. inversepass : Default false. Set to true for to take the reciprocal to the heading value
   f. goodpass : Number of hits required to get a good/excellent hit
   g. foulline_distance : Distance between the foul line and starfing target. Any shots fired beyond the strafe pit is not counted.

8. `RANGE:GetFoullineDistance(strafing_target, foulline_object)` : Distance between the starfing pit and it's corresponding foulline marker object. The value returned from this function can be used in the foulline_distance defined in the previous point.
9. `RANGE:SetInstructorRadio` and `RANGE:SetRangeControl` : This sets the frequency for the range. First one is for entering and leaving the range zone. The second one is for results on the range. These depend upon sound files to be put into the mission by the mission maker.
10. `SetBombtrackThreshold(radius)` : Tracks any bombs or missiles launched within a set radius in km.
11. `SetScoreBombDistance(radius)` : Sets the radius in meters in which bomb or missile impacts are recorded.
12. `SetDefaultPlayerSmokeBomb(boolean)` : Boolean to set whether munition impacts are smoked or not.

### Recovery Tanker

MOOSE Documentation : [Ops.RECOVERYTANKER](https://flightcontrol-master.github.io/MOOSE_DOCS_DEVELOP/Documentation/Ops.RecoveryTanker.html)

The module enables the creation of a recovery tanker and an AWACS plane to follow the carrier around. by default unlimited fuel is not set so the planes will land back on the carrier, respawn and will be launched again. This can be set via a function call for the tanker.

Main class is `RECOVERYTANKER`. This is used with the `New` function call, and the rest of the functions follow the class initialisation variable.

1. `NEW(carrier_unit_name, tanker_group_namme)` : Creates a new tanker/awacs on the specified carrier via the unit name from the template matching with the tanker/awacs group name. Multiple tankers/awacs can be assigned to the same carrier. This can create a cyclic tanker/awacs whenever one comes into land.
2. `SetCallsign(name_tanker, callsign_number)` : Sets the callsign of the tanker/awacs. The name of the tanker/awacs can either be set via MOOSE's callsign enumerator or as a number. It is 1-indexed so in the dropdown menu in the menu editor count the first callsign name as 1. The callsign number can be set between 1-9. If not defined MOOSE will default to 1 or the next corresponding digit available.
3. `SetTakeoffHot()` : Will spawn the plane with engines running. This reduces the time taken to spool up and launch.
4. `SetTACAN(tacan_value, tacan_morse, tacan_mode)` : Sets the tacan value, tacan morse value/text and model. For planes only mode *Y* works so by default this function sets the tacan to mode *Y*.
5. `SetRadio(radio_frequency, radio_modulation)` : Sets the radio frequency for the plane and radio modulation as either "FM" or "AM".
6. `SetSpeed(tas_knots)` : Set the speed of the plane in total air speed. There is a difference by about 20 knots at a minimum.
7. `SetAltitude(height_feet)` : Set the pattern altitude of the plane in feet.
8. `SetRacetrackDistances(distbow_nm, diststern_nm)` : Set the distance ahead and behind the bow and stern respectively of the ship.
9. `SetAWACS()` : Set the plane as an AWACS instead of a tanker. TACAN can be set for this type of plane also.
10. `Start()` : Initialise the plane.
11. `SetTACANoff()` : Turns off the tacan for the specified plane.
12. `SetLowFuelThreshold` : Sets the bingo state for the plane in percentage of total fuel.