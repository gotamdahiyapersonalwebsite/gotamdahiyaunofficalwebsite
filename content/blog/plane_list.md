+++
author = "(SN)Orion | vCSG-3 | Hornet School"
title = "Mission Editor Playable Plane List"
image = "img/slide/3.png"
date = "2024-05-13T00:00:00"
description = "Playable plane list"
tags = [
    "scripting"
]
+++

| Jet Planes | Piston Engine Planes | Helicopters |
| :--------: | :--------: | :---------: |
| Mig-21 Bis | P-51D-30-NA | AH-64D BLK.II |
| M-2000C | P-47D-40 | Mi-24P |
| JF-17 | I-16 | UH-1H |
| F-14B | Spitfire LF Mk. IX [CW] | Ka-50 |
| F-14A-135-GR | Bf 109 K-4 | Ka-50 III |
| F-15E S4+ | Fw 190 A-8 | Mi-8MMTV2 |
| F-16CM bl. 50 | Fw 190 D-9 | SA342 L/M/Minigun |
| F-5E-3 | Christen Eagle II | |
| A-10C II | Mosquito | |
| A-10A | | |
| AJS37 | | |
| AV-8B N/A | | |
| MB-339A | | |
| Su-27 | | |
| Su-33 | | |
| F-86F | | |
| Mig-19P | | | 
| Mig-29S | | |
| Mig-29G | | |
| L-39ZA | | |
| Mirage F1EE | | |
| C-101CC | | |
| J-11A | | |
| Su-25 | | |
| Su-25T | | |