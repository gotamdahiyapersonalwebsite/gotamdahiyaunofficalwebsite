+++
author = "(SN)Orion | vCSG-3 | Hornet School"
title = "SAM Trigger and SkyNet IADS"
image = "img/slide/5.png"
date = "2024-04-19T00:00:00"
description = "Documentation on setting up basic SAM engagement zones and using SkyNet to handle IADS"
tags = ["scripting"]
weight = 2
+++

## Introduction

There will be two methods discussed here,
1. Using triggers to activate and make the unit start emitting radar to track the planes.
2. Using SkyNet IADS to control certain SAM sites using a script in the background and EWR sites. This also takes care of SEAD/DEAD attacks

The first method can be used for SAM sites close to the friendly air-bases while the second method can be used for SAM sites slighly farther away from friendly airbases

### Triggers

This is a simple method and works with three triggers, one _1 ONCE_ and two _3 SWITCHED CONDITION_.

The following table provides an overview of the trigger,

| Trigger | Condition | Action |
| :-----: | :-------: | :----: |
| 1 ONCE (SAM SITE NAME, NO EVENT) | | UNIT EMISSION OFF (ALL Launchers, radars and shorad associated with SAM site) |
| 3 SWITCHED CONDITION (SAM SITE NAME, NO EVENT) | ALL OF COALITION OUT OF ZONE (COALITION, TRIGGER ZONE, UNIT TYPE) | UNIT EMISSION OFF (ALL Launchers, radars and shorad associated with the SAM site) |
| 3 SWITCHED CONDITION (SAM SITE NAME, NO EVENT) | PART OF COALITION IN ZONE (COALITION, TRIGGER ZONE, UNIT TYPE) | UNIT EMISSION ON (ALL Launchers, radars and shorad associated with the SAM site) |

The condition for the SWITCHED CONDITION will have to be set separately for each unit type.

For the SAM site group, alarm state has to be set to _RED_.

### SkyNet IADS

This is similar to MOOSE scripting which takes care of all the processing in the background relating to SAM site activation and usage dependent upon adversary coalitions' actions. Only one SAM type per group without all the extra stuff such as trucks is required for proper functioning of the SAM site with SkyNet.

EWR radar has the same restriction with one EWR per group and not more.

The [README](https://github.com/walder/Skynet-IADS/blob/master/README.md) has the best information for installing and basic instructions on usage.

## SHORAD Trigger

This is for randomly activating a SHORAD SAM Launcher. SHORAD are independent of Search and Track Radars.

| Trigger | Condition | Action |
| :-----: | :-------: | :----: |
| 1 ONCE (RANDOM 1, NO EVENT) | | FLAG SET VALUE (FLAG, MINIMUM VALUE, MAXIMUM VALUE) |
| 1 ONCE (SHORAD UNIT, NO EVENT) | FLAG IS MORE (FLAG, VALUE) | GROUP ACTIVATE (SHORAD UNIT) |