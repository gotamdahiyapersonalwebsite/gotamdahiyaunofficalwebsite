+++
author = "(SN)Orion"
title = "Autopilot Notes"
image = "img/slide/9.png"
date = "2024-11-21T00:00:00"
description = "Notes for autopilot systems for Airbus and Boeing Airliners."
tags = [
    "xplane12", "autopilot"
]
+++

## Airbus Autopilot

For the A330 use the following steps,
1. Before T/D set the FAF (final approach fix) altitude
2. At T/D set autopilot for managed descent.
3. Arm the approach phase from the MDCU.
4. Turn on LS for both PFDs.
5. Before FAF point, arm LOC and APPR autopilot modes
6. When the G/S indicator becomes level with the aircraft, the plane will guide itself in.
7. Disable autopilot modes close to the runway if it is visible

The above should match with the other Airbus aircraft due to similarity in systems from all.

## Boeing Autopilot

This is more hands on compared to the Airbus autopilot.

### After Startup
1. Arm the Flight Directors for both pilot and co-pilot.
2. Arm the autothrottle on the pilot's side.

### Before Takeoff
1. Set the altitude as specified by the ATC or the cruise altitude when flying without ATC.
2. Set airspeed to 200-250knots. Speed can be increased after passing 10,000ft.
3. Set N1 for throttle management.

### After Takeoff
1. Turn on the autopilot for the pilot's side.
2. Turn on LNAV to follow the waypoints unless directed by ATC to fly a certain heading.

### After reaching TOC
1. Turn on VNAV
2. Set altitude to approximate FAF if known, otherwise wait for ATC and ATIS to provide the approach.

### During descent
1. If the altitude was set already before TOD, then the plane will automatically land and descent.

### During final approach
1. Arm the APPR (approach) mode, the runway should appear on the PFD and the localiser bars should be visible.
2. The plane will first intercept the localiser and when it comes level with the G/S will it follow that.
3. On approaching minimums disengage autopilot and auto-throttle, land the plane manually.

## Embraer Autopilot

This is similar to the Airbus autopilot. Involves fewer buttons the FCS takes care of the rest.

### Before Takeoff
1. Arm the AP through the TOGA switch. This will also arm the autothrottle automatically when reaching a certain lever position

### During Takeoff
1. After 300ft AGL, LNAV will take over, and the AP can be enabled.
2. After 400ft AGL, VNAV will take over and the pilot can be hands off.

### After reaching TOC
1. Set the approximate altitude for FAF via pre-briefed approach or get it from ATIS/ATC.

### During Final Approach
1. Arm the APPR mode for the FMS.
2. FMS will automatically tune the ILS and VOR radios and fly the plave after intercepting the localiser and the glide slope.

## MD-11

This is only for the MD-11 aircraft. The MD-80 and MD-11 share different autopilot modes and switchology. The MD-11 autopilot is a cross between an Airbus and a Boeing autopilot. Not as many as buttons to press as a Boeing but not as automatic as an Airbus.

### During Startup
1. Assign the FL and cost index in the FMS.
2. Set the cruising altitude on the panel.

### Before Takeoff
1. Click on Prof on the panel to arm LNAV and Auto thrust.
2. When wheels are off the ground, change heading to NAV mode, for the plane to follow the assigned route.

### After reaching TOC
1. Change the assigned altitude to the FAF altitude.

### During Final Approach
1. Arm APPR/LAND mode. This will arm LOC and GS modes, which will automatically take over when the horizontal and vertical localisers are picked up by the plane. 