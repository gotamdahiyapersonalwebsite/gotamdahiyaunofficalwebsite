+++
author = "(SN)Orion"
title = "VATSIM Planning Notes"
image = "img/slide/9.png"
date = "2024-11-20T00:00:00"
description = "VATSIM Planning Notes"
tags = [
    "xplane12", "vatsim"
]
+++

## Flight Planning
1. East and North is on an odd flight level. (Heading 0 - 179)
2. West and South is on an even flight level. (Heading 180 - 359)
3. Aviate, Navigate, Communicate.
4. Put new pilot in remarks section of the flight plan so that the controllers speak slower with you and can hold your hand.
5. Don't be afraid of the controllers, they are understanding and other pilots will help.

## ATC Communications

### ATC Clearance Delivery

There are 5 basic things required by the pilot to say
1. Who we are => Callsign
2. What we are => Type of aircraft
3. Where we are => Stand/Gate. Location of the aircraft at the airport
4. What we have => ATIS information and QNH of the airport
5. What we want => Clearance to destination. Clearance Delivery will provide SID to use.

### ATC Ground

Controls the taxing of aircraft on taxiways at the airport. Does not control the crossing of runways, that lies with the Tower.

Taxi instructions can either come in broken which involves hold points or will be the entire path from stand/gate to runway hold short point.

Ground will clear an arrival to a stand via direct or broken instructions.

### ATC Tower

Controls the crossing of runways, take offs, landings and go-arounds. Main focus is the runway area and all flights associated with it.

Tower will clear the pilot to land after handover from Approach controller.

### ATC Approach

Guides airplanes into and out of airports in an orderly fashion. Approach controllers can control your speed, heading and altitude.

Approach controllers can either keep the pilot on the STAR or guide them manually for spacing. Pilot has to follow their manual instructions if not on a STAR.

### ATC Radar

They control vast areas of land, if active need them for an initial check-in.

Example statement : "London Control, this is Easy 123, inbound to Waypoint XXX at Flight Level XXX".

For arrival, ask radar for decent and will clear the pilot onto a STAR and a runway.

### ATC Oceanic Clearance

This is to be provided to controllers to allow for passage over vast stretches of sea. Position reports are given via time and not distance. The controller will check the SELCAL so that they may alert for a message. A good example and website for creating the position report is provided [here](https://www.vatnz.net/pilots/oceanic-report-tool/)

Position reports are provided upon passing each waypoint or after 45 minutes which ever comes first.