+++
title = 'DCS Media'
image = ""
date = "2023-01-08T00:00:00"
description = 'Collection of images from DCS'
tags = [
    "dcs-media"
]
+++

{{< slider src="/data/dcs-media.json" >}}

<!-- | ![Figure 1](/img/world_map.jpg) |
|:--:|
| *Figure 1* |
 -->
 <!-- image = 'img/Operation_Onyx_Start.jpg' -->
 <!-- {{< display img="/img/orbat/1_COY_HQ.png" >}} -->