+++
title = 'About Us'
slug = 'aboutus'
image = '/images/aboutus/det_207_logo.png'
description = 'An introduction to Detachment 207'
disableComments = true
+++

## Introduction

Detachment 207 MILSIM group was founded by a group of ARMA 3 veterans with the purpose of providing a serious and quality experience for players in Australia, New Zealand and SE Asia. 

Our current make up is a direct action shock platoon (Commando inspired) with a weapons group, ISR elements and an aviation element. 

We host Training & Operations on a weekly basis for our personnel and maintain a chain of command and high level of training to ensure our members are ready for all scenarios and situations.

What we value : 
1. More shooting and less saluting. We like to maximise game play time in ops.
2. Professional mastery, we want our sections to impress.
3. Sef-ownershipe, tools are provided, use them to your best advantage.
4. Everyone's a mate, from the top brass to the newest recruit, expect to be treated like more than just a number. 
5. A wealth of experience, no matter where you come from, everyone has something to teach.

A brief overview of our orbat can be found [here]({{< relref "/orbat.md" >}}).

## Basic Rules

1. Be able to speak English proficiently.
2. Have a working microhone.
3. Legal copy of Arma 3.
4. Teamspeak
5. **Be 18+ years of age upon joining.**

## Application

The link to the Detachment 207 discord : https://discord.gg/KzzFpDm

There are two stages to join Detachment 207,

1. First is the application form and an informal chat which gauges the applicants interest and whether Detachment 207 will be a good fit for the applicant. After passing this stage, the applicant is promoted to a *Prospective Member* and *Recruit*.
2. - Second step is a training in which the recruit is taken through formations, anti-tank munitions, bounding, medical, ACE, TFAR and other mods which we use so that the recruit is ready for operations.
   - After completing *two operations* with 207, the recruit is given our beret at the end of the second operation, after which they can choose which section they would like to be assigned to.