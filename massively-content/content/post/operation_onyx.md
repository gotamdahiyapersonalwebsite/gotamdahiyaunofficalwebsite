+++
title = 'Operation Onyx'
slug = 'post2'
image = 'images/Operation_Onyx_Start.jpg'
date = "2023-01-08T00:00:00"
description = 'Detachment 207 is tasked with inserting into Vidda stealthly to retrieve intel regarding Achmed and if possible eliminate him'
disqus_identifier = '10'
+++

## De-Brief

Achmed is dead, and Operation Onyx is a success. After Achmed eluded capture, a stealth insertion into a Corporation bunker led to the swift death of the notorious man. Promptly following the death of Achmed, Corporation forces attempted to eliminate D-207 and forced D-207 to leave the region. Analysis of items recovered from his body shows a dangerous concoction of chemicals and a dangerous level of radiation. These chemicals included an unknown and highly toxic synthetic substance, which is believed to have been created by the Corporation. 

Before being pushed out of Vidda, D-207 was able to find intel leading to a Corporation base in “MGR 36P”. This location has been identified as Kujari; a transport hub discovered and eliminated by D-207 during the events of Operation Helsing nearly a year ago. This is displayed roughly as the red square on Figure 1 below.

## Implications

The discovery that The Corporation has reestablished its control of Kujari so quickly, and without the international intelligence communities' awareness, raises concerns about the mysterious organisation's capability. Detachment 207 will be entering the region once more to investigate The Corporation. Luckily, D-207’s previous efforts in the region and subsequent aid provided will likely have not been forgotten by the populace, and provide a level of an existing trust.

| ![Figure 1](/images/world_map.jpg) |
|:--:|
| *Figure 1* |

**Key**:
- Red indicated active objectives
- Yellow indicates previous hostile objectives
- Blue indicates ally locations
